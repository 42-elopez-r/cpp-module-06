/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/17 12:43:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 13:25:07 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MultiCaster.hpp"
#include <iostream>

using std::cout; using std::endl;
using std::cerr;

void
showRepresentations(MultiCaster* mc)
{
	char c;
	int i;
	float f;
	double d;

	std::fixed(cout);
	cout.precision(1);

	cout << "char: ";
	try
	{
		c = mc->toChar();
		cout << "'" << c << "'" << endl;
	}
	catch (MultiCaster::InvalidConversionException& e)
	{
		cout << e.what() << endl;
	}

	cout << "int: ";
	try
	{
		i = mc->toInt();
		cout << i << endl;
	}
	catch (MultiCaster::InvalidConversionException& e)
	{
		cout << e.what() << endl;
	}

	cout << "float: ";
	try
	{
		f = mc->toFloat();
		cout << f << "f" << endl;
	}
	catch (MultiCaster::InvalidConversionException& e)
	{
		cout << e.what() << endl;
	}

	cout << "double: ";
	try
	{
		d = mc->toDouble();
		cout << d << endl;
	}
	catch (MultiCaster::InvalidConversionException& e)
	{
		cout << e.what() << endl;
	}
}

int
main(int argc, char* argv[])
{
	MultiCaster* mc;

	if (argc != 2)
	{
		cerr << "Usage: " << argv[0] << " LITERAL_VALUE" << endl;
		return (1);
	}
	mc = new MultiCaster(argv[1]);

	showRepresentations(mc);

	delete mc;
	return (0);
}
