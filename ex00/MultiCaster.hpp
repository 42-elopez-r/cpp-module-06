/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MultiCaster.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/15 20:16:58 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/16 21:08:25 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MULTICASTER_HPP
#define MULTICASTER_HPP

#include <string>
#include <exception>

using std::string;

class MultiCaster
{
	public:
		MultiCaster(const string& original);
		MultiCaster(const MultiCaster& multiCaster);
		~MultiCaster() {}
		MultiCaster& operator=(const MultiCaster& multiCaster);
		char toChar() const;
		int toInt() const;
		float toFloat() const;
		double toDouble() const;

		class InvalidConversionException: std::exception
		{
			public:
				InvalidConversionException(const string& msg);
				~InvalidConversionException() throw() {}
				const char* what() const throw();
			private:
				InvalidConversionException();

				string _msg;
		};

	private:
		enum type_e {CHAR, INT, FLOAT, DOUBLE, NOPE};

		string _original;
		type_e _type;

		MultiCaster();
		bool testChar() const;
		bool testInt() const;
		bool testFloat() const;
		bool testDouble() const;
};

#endif
