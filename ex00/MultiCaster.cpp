/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MultiCaster.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/15 20:28:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 17:59:16 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MultiCaster.hpp"

#include <climits>
#include <cstdlib>
#include <sstream>

#define isnan(x) ((x) != (x))

/*
 * This constructor stores the string to convert and determines its type.
 */
MultiCaster::MultiCaster(const string& original): _original(original)
{
	if (testChar())
		_type = CHAR;
	else if (testInt())
		_type = INT;
	else if (testFloat())
		_type = FLOAT;
	else if (testDouble())
		_type = DOUBLE;
	else
		_type = NOPE;
}

/*
 * Copy constructor
 */
MultiCaster::MultiCaster(const MultiCaster& multiCaster)
{
	*this = multiCaster;
}

/*
 * Assignation operator overload.
 */
MultiCaster&
MultiCaster::operator=(const MultiCaster& multiCaster)
{
	_original = multiCaster._original;
	_type = multiCaster._type;

	return (*this);
}

/*
 * Returns char representation of _original. Throws InvalidConversionException
 * in case of error.
 */
char
MultiCaster::toChar() const
{
	int numeric_repr;
	double double_repr;

	switch (_type)
	{
		case CHAR:
			return (_original[1]);
		case INT:
			numeric_repr = atoi(_original.c_str());
			if (numeric_repr < 0 || numeric_repr > 127)
				throw InvalidConversionException("Impossible");
			if (isprint(numeric_repr))
				return (static_cast<char>(numeric_repr));
			else
				throw InvalidConversionException("Non displayable");
			break;
		case FLOAT:
		case DOUBLE:
			double_repr = atof(_original.c_str());
			numeric_repr = static_cast<int>(double_repr);
			// Check for decimals
			if (double_repr - numeric_repr != 0 ||
					double_repr < INT_MIN || double_repr > INT_MAX ||
					numeric_repr < 0 || numeric_repr > 127)
				throw InvalidConversionException("Impossible");
			if (isprint(numeric_repr))
				return (static_cast<char>(numeric_repr));
			else
				throw InvalidConversionException("Non displayable");
		default:
			throw InvalidConversionException("Error");
	}
}

/*
 * Returns integer representation of _original. Throws InvalidConversionException
 * in case of error.
 */
int
MultiCaster::toInt() const
{
	double double_repr;

	switch (_type)
	{
		case CHAR:
			return (static_cast<int>(_original[1]));
		case INT:
			return (atoi(_original.c_str()));
		case FLOAT:
		case DOUBLE:
			double_repr = atof(_original.c_str());
			if (isnan(double_repr) || double_repr < INT_MIN || double_repr > INT_MAX)
				throw InvalidConversionException("Impossible");
			return (static_cast<int>(double_repr));
		default:
			throw InvalidConversionException("Error");
	}
}

/*
 * Returns float representation of _original. Throws InvalidConversionException
 * in case of error.
 */
float
MultiCaster::toFloat() const
{
	switch (_type)
	{
		case CHAR:
			return (static_cast<float>(_original[1]));
		case INT:
			return (static_cast<float>(atoi(_original.c_str())));
		case FLOAT:
		case DOUBLE:
			return (static_cast<float>(atof(_original.c_str())));
		default:
			throw InvalidConversionException("Error");
	}
}

/*
 * Returns double representation of _original. Throws InvalidConversionException
 * in case of error.
 */
double
MultiCaster::toDouble() const
{
	switch (_type)
	{
		case CHAR:
			return (static_cast<double>(_original[1]));
		case INT:
			return (static_cast<double>(atoi(_original.c_str())));
		case FLOAT:
		case DOUBLE:
			return (atof(_original.c_str()));
		default:
			throw InvalidConversionException("Error");
	}
}

/*
 * InvalidConversionException constructor. Receives message to be output by what().
 */
MultiCaster::InvalidConversionException::InvalidConversionException(const string& msg)
{
	_msg = msg;
}

/*
 * std::exception::what() overload.
 */
const char*
MultiCaster::InvalidConversionException::what() const throw()
{
	return (_msg.c_str());
}

/*
 * This method will test if _original is a character, returning true if it is.
 */
bool
MultiCaster::testChar() const
{
	if (_original.length() != 3)
		return (false);
	return (_original[0] == '\'' && _original[2] == '\'');
}

/*
 * This method will test if _original is an integer, returning true if it is.
 */
bool
MultiCaster::testInt() const
{
	long lon;

	for (size_t i = (_original[0] == '-' ? 1 : 0); i < _original.length(); i++)
		if (!isdigit(_original[i]))
			return (false);
	if (_original[0] == '-' && (_original.length() > 11 || _original.length() == 1))
		return (false);
	if (_original[0] != '-' && _original.length() > 10)
		return (false);

	lon = atol(_original.c_str());
	return (lon <= INT_MAX && lon >= INT_MIN);
}

/*
 * This method will test if _original is a float, returning true if it is.
 */
bool
MultiCaster::testFloat() const
{
	bool dotFound;

	if (_original == "-inff" || _original == "+inff" || _original == "nanf")
		return (true);

	if (_original[0] != '-' && !isdigit(_original[0]))
		return (false);

	dotFound = false;
	for (size_t i = 1; i < _original.length() - 1; i++)
	{
		if (!dotFound && _original[i] == '.')
		{
			dotFound = true;
			if (i + 1 == _original.length() || !isdigit(_original[i + 1]))
				return (false);
		}
		else if (!isdigit(_original[i]))
			return (false);
	}

	return (dotFound && _original[_original.length() - 1] == 'f');
}

/*
 * This method will test if _original is a double, returning true if it is.
 */
bool
MultiCaster::testDouble() const
{
	bool dotFound;

	if (_original == "-inf" || _original == "+inf" || _original == "nan")
		return (true);

	if (_original[0] != '-' && !isdigit(_original[0]))
		return (false);

	dotFound = false;
	for (size_t i = 1; i < _original.length(); i++)
	{
		if (!dotFound && _original[i] == '.')
		{
			dotFound = true;
			if (i + 1 == _original.length() || !isdigit(_original[i + 1]))
				return (false);
		}
		else if (!isdigit(_original[i]))
			return (false);
	}

	return (dotFound);
}
