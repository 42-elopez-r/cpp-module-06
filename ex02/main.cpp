/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/17 17:00:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 17:27:34 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

/*
 * This function returns a heap allocated instance of (randomly selected)
 * A, B or C, as a Base.
 */
static Base*
generate(void)
{
	switch (rand() % 3)
	{
		case 0:
			return (new A);
		case 1:
			return (new B);
		default:
			return (new C);
	}
}

/*
 * This function takes a pointer to a A, B or C object and identifies it.
 */
static void
identify(Base* p)
{
	if (dynamic_cast<A*>(p))
		cout << "A" << endl;
	else if (dynamic_cast<B*>(p))
		cout << "B" << endl;
	else if (dynamic_cast<C*>(p))
		cout << "C" << endl;
	else
		cout << "Other" << endl;
}

/*
 * This function takes a reference to a A, B or C object and identifies it.
 */
static void
identify(Base& p)
{
	try
	{
		A& a = dynamic_cast<A&>(p);
		cout << "A" << endl;
		(void)a;
		return;
	}
	catch (std::bad_cast& bc)
	{}

	try
	{
		B& b = dynamic_cast<B&>(p);
		cout << "B" << endl;
		(void)b;
		return;
	}
	catch (std::bad_cast& bc)
	{}

	try
	{
		C& c = dynamic_cast<C&>(p);
		cout << "C" << endl;
		(void)c;
		return;
	}
	catch (std::bad_cast& bc)
	{}
	
	cout << "Other" << endl;
}

int
main()
{
	A a;
	B b;
	C c;
	Base* aux;

	cout << "Test identify(Base*):" << endl;
	cout << "Type A: ";
	identify(&a);
	cout << "Type B: ";
	identify(&b);
	cout << "Type C: ";
	identify(&c);

	cout << endl << "Test identify(Base&):" << endl;
	cout << "Type A: ";
	identify(a);
	cout << "Type B: ";
	identify(b);
	cout << "Type C: ";
	identify(c);

	cout << endl << "Test generate():" << endl;
	srand(time(NULL));
	for (int i = 0; i < 5; i++)
	{
		aux = generate();
		identify(aux);
		delete aux;
	}

	return (0);
}
