/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serialization.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/17 16:18:04 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 16:27:08 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERIALIZATION_HPP
#define SERIALIZATION_HPP

#include <string>
#include <ostream>

using std::string; using std::ostream;

struct Data
{
	string str;
	int num;
	char c;
};

ostream& operator<<(ostream& os, const Data& data);

uintptr_t
serialize(Data* ptr);

Data*
deserialize(uintptr_t raw);

#endif
