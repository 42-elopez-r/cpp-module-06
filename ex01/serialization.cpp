/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serialization.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/17 16:21:16 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 16:42:10 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serialization.hpp"
#include <iostream>

using std::endl;

/*
 * << operator overflow for outputting a Data struct.
 */
ostream&
operator<<(ostream& os, const Data& data)
{
	os << "Pointer: " << &data << endl;
	os << "str: " << data.str << endl;
	os << "num: " << data.num << endl;
	os << "c: " << data.c << endl;

	return (os);
}

/*
 * Returns the passed Data pointer as an unsigned integer.
 */
uintptr_t
serialize(Data* ptr)
{
	return (reinterpret_cast<uintptr_t>(ptr));
}

/*
 * Returns the passed unsigned integer as a Data pointer.
 */
Data*
deserialize(uintptr_t raw)
{
	return (reinterpret_cast<Data*>(raw));
}
