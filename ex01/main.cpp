/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/17 16:22:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/17 16:37:37 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "serialization.hpp"
#include <iostream>

using std::cout; using std::endl;

int
main()
{
	Data original;
	uintptr_t raw;
	Data* deserialized;

	original.str = "Adiós mundo";
	original.num = 42;
	original.c = 'M';

	cout << "Original:" << endl << original;

	raw = serialize(&original);
	cout << endl << "Raw: " << raw;
	cout << "(in hex: " << std::hex << raw << std::dec << ")" << endl;

	deserialized = deserialize(raw);
	cout << endl << "Deserialized:" << endl << *deserialized;

	return (0);
}
